<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>ICC - BBS home</title>


    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/bbs_style.css"/>


</head>
<body>

    <?php
    session_start();
    if(!isset($_SESSION['USERID'])){
        header("Location: loginfail.php");
        exit;
    }
    ?>

    <div id="container">

        <div id="header">
            <a href="index.html"><img border="0" src="images/headerMain.png" height="100"></a>
            <div class="headerNavArea">
                <ul id="headerNav">
                    <li><a href="index.html">TOP</a></li>
                    <li><a href="greetings.html">GREETINGS</a></li>
                    <li><a href="about.html">ABOUT ICC</a></li>
                    <li><a href="theme.html">THEME</a></li>
                    <li><a href="schedule.html">SCHEDULE</a></li>
                </ul>
            </div>

        </div>

        <div id="sidemenu">

            <ul id="sideNav">
                <li><a href="photo.html">PHOTO<br/><br/><img border="0" src="images/camera.png" width="50"></a></li>
                <li><a href="speaker.html">SPEAKERS<br/><br/><img border="0" src="images/speaker.png" width="50"></a></li>
                <li><a href="clipboard.html">FACULTY<br /><br /><img border="0" src="images/clipboard.png" width="50"></a></li>
                <li><a href="support.html">SUPPORT<br /><br /><img border="0" src="images/support.png" width="50"></a></li>
            </ul>
        </div>


        <div id="contents">

            <?php

                $link = mysql_connect("home.nda.ac.jp","s60127","0000");
                mysql_select_db("s60127");

                $dispid = $_SESSION['USERID'];

                $displayresult = mysql_query("select * from userid where userid=\"$dispid\" ");
                $disprow = mysql_fetch_array($displayresult);

                echo "<P>ようこそ　".$disprow["username"]."　さん</P>";
            ?>

            <form action="iccbbs_sub.php">

                <textarea name="message" cols="80" rows="4">message here</textarea><br/>
                <!--ファイルをアップロード　＞＞<input type="file" name="upfile" size="25"/><br/>-->
                <input type="submit" value="投稿"/>

            </form>
            <br/>

            <?php


            $result = mysql_query("select * from iccbbs order by msno desc");


            while($row = mysql_fetch_array($result)){
                echo "<div class=\"messageBox\">";
                if($row["country"] == 'ja'){
                    $flagid = 'flag093';
                }
                echo "<p>".$row["msno"]."　<img src=\"images/flags/".$flagid.".png\" width=\"17\"><font color=\"#1d30c4\">".$row["username"]."</font></p>";
                echo "<p>".$row["message"]."</p>";
                echo "<a href=\"uploads/".$row["files"]."\">".$row["files"]."</a>";
                echo "</div>";
            }

            mysql_close($link);
            
            ?>
            
        </div>

    </div>

    <div id="footer">
        <address>Copyright (c) ICC Committie 2015 -- all rights received.</address>
    </div>

</body>
</html>